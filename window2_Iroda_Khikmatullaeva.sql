WITH yearly_sales AS (
    SELECT
        p.prod_subcategory,
        EXTRACT(YEAR FROM t.time_id) AS year,
        SUM(s.amount_sold) AS total_sales
    FROM sh.products p
    JOIN sh.sales s ON p.prod_id = s.prod_id
    JOIN sh.times t ON s.time_id = t.time_id
    WHERE EXTRACT(YEAR FROM t.time_id) BETWEEN 1998 AND 2001
    GROUP BY p.prod_subcategory, EXTRACT(YEAR FROM t.time_id)),
sales_on_prev_year AS (
    SELECT
        prod_subcategory,
        year,
        total_sales,
        LAG(total_sales) OVER (PARTITION BY prod_subcategory ORDER BY year) AS prev_year_sales
    FROM yearly_sales)

SELECT DISTINCT prod_subcategory
FROM (SELECT
        prod_subcategory,
        SUM(CASE WHEN total_sales > COALESCE(prev_year_sales, 0) THEN 1 ELSE 0 END) AS years_higher
    FROM sales_on_prev_year
    WHERE year BETWEEN 1998 AND 2001
    GROUP BY prod_subcategory) subcat_sales
WHERE years_higher = 4;